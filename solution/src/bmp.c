#include "../header/bmp_utils.h"
#include "../header/image.h"

#include <stdio.h>

#define BMP_HEADER_TYPE 19778
#define BMP_HEADER_PLANES 1
#define BMP_HEADER_INFO_SIZE 40


static inline size_t get_padding(size_t width) {
	return (4 - (size_t)((width * sizeof(struct pixel)) % 4));
}

static enum read_status get_header(FILE* file, struct bmp_header* header) {
	if (fread(header, sizeof(struct bmp_header), 1, file)) {
		return READ_OK;
	} else {
		return READ_INVALID_HEADER;
	}
}

static enum read_status get_pixels(FILE* file, struct image* img) {
	struct image new = create_image(img->width, img->height);
	if (new.pixels == NULL) return READ_INVALID_BITS; 
	for (size_t i = 0; i < new.height; ++i) {
		if (fread(new.pixels + (i * new.width), sizeof(struct pixel), new.width, file) != new.width) {
			free(new.pixels);
			return READ_INVALID_SIGNATURE;
		}
		if (fseek(file, get_padding(new.width), SEEK_CUR) != 0) {
			free(new.pixels);
			return READ_INVALID_SIGNATURE;
		}
	}
	img->pixels = new.pixels;
	return READ_OK;
}

static struct bmp_header create_header(const struct image* img) {
	uint32_t image_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	return (struct bmp_header) {
		.bfType = BMP_HEADER_TYPE,
            	.bfileSize = sizeof(struct bmp_header) + image_size,
            	.bfReserved = 0,
            	.bOffBits = sizeof(struct bmp_header),
            	.biSize = BMP_HEADER_INFO_SIZE,
            	.biWidth = img->width,
            	.biHeight = img->height,
            	.biPlanes = BMP_HEADER_PLANES,
            	.biBitCount = 24,
            	.biCompression = 0,
            	.biSizeImage = image_size,
            	.biXPelsPerMeter = 0,
            	.biYPelsPerMeter = 0,
            	.biClrUsed = 0,
            	.biClrImportant = 0
	};
}

static enum write_status write_header(FILE *file, struct bmp_header* header) {
	if (fwrite(header, sizeof(struct bmp_header), 1, file) == 1) {
		return WRITE_OK;
	} else {
		return WRITE_ERROR;
	}
}

static enum write_status write_pixels(FILE* file, const struct image* img) {
	for (size_t i = 0; i < img->height; ++i) {
		if (fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, file) != img->width){
			return WRITE_ERROR;
		}
		if (fseek(file, get_padding(img->width), SEEK_CUR) != 0) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = {0};
	if (get_header(in, &header) == READ_OK) {
		img->width = header.biWidth;
		img->height = header.biHeight;
		return get_pixels(in, img);
	} else {
		return READ_INVALID_SIGNATURE;
	}
}


enum write_status to_bmp(FILE* out, const struct image *img) {
	struct bmp_header header = create_header(img);
	if (write_header(out, &header) == WRITE_OK) {
		return write_pixels(out, img);
	} else {
		return WRITE_ERROR;
	}

}
