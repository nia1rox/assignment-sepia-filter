#include "../header/sepia.h"

struct pixel transform_pixel(const struct pixel pix) {
    uint8_t r = (pix.r * 0.393) + (pix.g * 0.769) + (pix.b * 0.189);
    uint8_t g = (pix.r * 0.349) + (pix.g * 0.686) + (pix.b * 0.168);
    uint8_t b = (pix.r * 0.272) + (pix.g * 0.534) + (pix.b * 0.131);
    if (r > 255) r = 255;
    if (g > 255) g = 255;
    if (b > 255) b = 255;
    return (struct pixel) {r, g, b};
}

struct image sepia_c(const struct image *src ) {
    struct image copy_image = create_image(src->width,src->height);
    if (copy_image.pixels == NULL) {
    	return (struct image){0};
    }
    for (size_t i = 0; i < src->width * src->height; ++i) {
       copy_image.pixels[i] = transform_pixel(src->pixels[i]);
    }
    return copy_image;
}

struct image sepia_asm(struct image* img) {
	struct image new_image = create_image(img -> height, img -> width);
	float r[4], g[4], b[4];
	uint8_t result[12];
	struct pixel* pixel;

	for(uint64_t i = 0; i < img -> height * img -> width; i += 4) {
		for(uint8_t j = 0; j < 4; j++) {
			pixel = (img -> pixels + i + j);
			r[j] = pixel -> r;
			g[j] = pixel -> g;
			b[j] = pixel -> b;			
		}

		sepia_native(r, g, b, result);

		for(uint8_t k = 0; k < 4; k++) {
			pixel = (new_image.pixels + i + k);
			pixel -> r = *(result + 3 * k);
			pixel -> g = *(result + 3 * k + 1);
			pixel -> b = *(result + 3 * k + 2);
		}
	}
	return new_image;
}
