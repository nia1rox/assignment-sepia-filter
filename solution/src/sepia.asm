%define r rdx
%define g rsi
%define b rdi
%define result rcx

%macro correct_args 0

add r, 4
add g, 4
add b, 4
add result, 4

%endmacro


c1_0: dd 0.272, 0.349, 0.393, 0.272
c1_1: dd 0.543, 0.686, 0.769, 0.543
c1_2: dd 0.131, 0.168, 0.189, 0.131

c2_0: dd 0.349, 0.393, 0.272, 0.349
c2_1: dd 0.686, 0.769, 0.543, 0.686
c2_2: dd 0.168, 0.189, 0.131, 0.168

c3_0: dd 0.393, 0.272, 0.349, 0.393
c3_1: dd 0.769, 0.543, 0.686, 0.769
c3_2: dd 0.189, 0.131, 0.168, 0.189

check: dd 255, 255, 255, 255

section .text
global sepia_native

return_value:
	cvtps2dq xmm0, xmm0 
	pminud xmm0, [check] 
	
	pextrb [result], xmm0, 0
	pextrb [result + 1], xmm0, 4
	pextrb [result + 2], xmm0, 8
	pextrb [result + 3], xmm0, 12
	ret

sepia_native:

movdqu xmm3, [c1_0]
movdqu xmm4, [c1_1]
movdqu xmm5, [c1_2]

movdqu xmm2, [r]
movdqu xmm1, [g]
movdqu xmm0, [b]

shufps xmm0, xmm0, 0x01
shufps xmm1, xmm1, 0x01
shufps xmm2, xmm2, 0x01

mulps xmm0, xmm5 
mulps xmm1, xmm4 
mulps xmm2, xmm3
addps xmm0, xmm1
addps xmm0, xmm2 
call return_value
correct_args

movdqu xmm3, [c2_0]
movdqu xmm4, [c2_1]
movdqu xmm5, [c2_2]

movdqu xmm2, [r]
movdqu xmm1, [g]
movdqu xmm0, [b]


shufps xmm0, xmm0, 0x05
shufps xmm1, xmm1, 0x05
shufps xmm2, xmm2, 0x05

mulps xmm0, xmm5
mulps xmm1, xmm4
mulps xmm2, xmm3

addps xmm0, xmm1
addps xmm0, xmm2

call return_value
correct_args

movdqu xmm3, [c3_0]
movdqu xmm4, [c3_1]
movdqu xmm5, [c3_2]

movdqu xmm2, [r]
movdqu xmm1, [g]
movdqu xmm0, [b]

shufps xmm0, xmm0, 0x15
shufps xmm1, xmm1, 0x15
shufps xmm2, xmm2, 0x15

mulps xmm0, xmm5
mulps xmm1, xmm4
mulps xmm2, xmm3

addps xmm0, xmm1
addps xmm0, xmm2

call return_value
