#include "./image.h"

struct image sepia_c(const struct image *src);
struct image sepia_asm(struct image* img);
void sepia_native(float* r, float* g, float* b, uint8_t* result);
